DolphinEntity = Core.class( Sprite )

function DolphinEntity:init()
end

function DolphinEntity:Setup()
	self.textures = {	
		default = Texture.new( "Graphics/InGame/sprite_dolphin.png" ),
		jumping = Texture.new( "Graphics/InGame/sprite_dolphin_jumping.png" )
		}
	self.maxY = SCREEN_HEIGHT - 55
	self.bitmap = Bitmap.new( self.textures.default )
	self.position = { x = 0, y = self.maxY }
	self.dimensions = { width = 100, height = 56 }
	
	self.velocityY = 0
	self.jumpVelocity = -8
	self.falling = false
	self.gravity = 0.25
	
	self.launchCount = 0
	
	self.lives = 0
	self.heartfloat = 0
	self.heartinc = 0.1
	self.heart = Bitmap.new( Texture.new( "Graphics/InGame/trinket_heart.png" ) )
	
	self.dead = false
	
	self:Clear()
	
	self.bitmap:setPosition( self.position.x, self.position.y )
	self.bitmap:addEventListener( Event.ENTER_FRAME, self.Update, self )	
end

function DolphinEntity:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.bitmap:removeEventListener( Event.ENTER_FRAME, self.Update, self )	
end

function DolphinEntity:GetCollisionRegion()
	local collidable = {
		position = { x = self.position.x + 10, y = self.position.y },
		dimensions = { width = self.dimensions.width - 10, height = self.dimensions.height }
	}
	
	return collidable
end

function DolphinEntity:Lose()
	self.dead = true
end

function DolphinEntity:IsDead()
	return self.dead == true
end

function DolphinEntity:ExtraLifeCount()
	return self.lives
end

function DolphinEntity:RemoveExtraLife()
	self.lives = 0
end

function DolphinEntity:AddExtraLife()
	self.lives = 1
end

function DolphinEntity:Update()
	if self.dead == true then
		self.position.y = self.position.y + self.velocityY
		self.velocityY = self.velocityY + self.gravity
		
	elseif self.falling == true then
		self.position.y = self.position.y + self.velocityY
		self.velocityY = self.velocityY + self.gravity
	elseif self.launchCount <= 0 then
		self.bitmap:setTexture( self.textures.default )
	end
	
	if self.launchCount > 0 then
		self.launchCount = self.launchCount - 1
	end
	
	if self.lives > 0 then
		self.heart:setPosition( self.position.x, self.position.y - 10 + self.heartfloat )
		if stage:contains( self.heart ) == false then stage:addChild( self.heart ) end
		self.heartfloat = self.heartfloat + self.heartinc
		if self.heartfloat >= 2.0 or self.heartfloat <= -2.0 then
			self.heartinc = -self.heartinc
		end
	else
		if stage:contains( self.heart ) == true then stage:removeChild( self.heart ) end		
	end
	
	self.bitmap:setPosition( self.position.x, self.position.y )
end

function DolphinEntity:IsFalling( dolphin )
	if self.dead == true then
		
	
	elseif self.position.y >= self.maxY then
		self.falling = false
		self.velocityY = 0
		self.secondJump = false
		self.position.y = self.maxY
	else
		self.falling = true
	end
end

function DolphinEntity:Draw()
	stage:addChild( self.bitmap )
end

function DolphinEntity:Clear()
	if stage:contains( self.bitmap ) == true then stage:removeChild( self.bitmap ) end
	if stage:contains( self.heart ) == true then stage:removeChild( self.heart ) end
end 

function DolphinEntity:Jump()
	if self.dead == false and self.falling == false then
		self.velocityY = self.jumpVelocity
		self.falling = true
		self.position.y = self.position.y + self.velocityY
		self.bitmap:setTexture( self.textures.jumping )
	end
end

function DolphinEntity:Launch()
	self.bitmap:setTexture( self.textures.jumping )
	self.launchCount = 20
end