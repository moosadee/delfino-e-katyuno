TileSelectState = Core.class()

function TileSelectState:init( options )
	self.name = options.name
	TileSelectState.currentBrush = nil
	TileSelectState.currentBrushName = nil
	TileSelectState.currentBrushMovement = nil
end

function TileSelectState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_checker.png" ),
		
		-- Util tiles
		tileEmpty = Texture.new( "Graphics/UserInterface/tile_empty.png" ),
		tileHeart = Texture.new( "Graphics/InGame/trinket_heart.png" ),
		tileEnd = Texture.new( "Graphics/InGame/tile_ending.png" ),
		
		-- Happy things
		tileTea = Texture.new( "Graphics/InGame/trinket_tea.png" ),
		tileBell = Texture.new( "Graphics/InGame/trinket_bell.png" ),
		tileFish = Texture.new( "Graphics/InGame/trinket_fish.png" ),
		tileLotus = Texture.new( "Graphics/InGame/trinket_lotus.png" ),
		tileBalloon = Texture.new( "Graphics/InGame/trinket_balloon.png" ),
		tileAcorn = Texture.new( "Graphics/InGame/trinket_acorn.png" ),
		tileFeather = Texture.new( "Graphics/InGame/trinket_feather.png" ),
		tileCandy = Texture.new( "Graphics/InGame/trinket_candy.png" ),
		tileFrog = Texture.new( "Graphics/InGame/trinket_frog.png" ),
		tilePenguin = Texture.new( "Graphics/InGame/trinket_penguin.png" ),
		tileSun = Texture.new( "Graphics/InGame/trinket_sun.png" ),
		tileButterfly = Texture.new( "Graphics/InGame/trinket_butterfly.png" ),
		
		-- Bad things
		tileSmallRock = Texture.new( "Graphics/InGame/obstacle_rock1.png" ),
		tileBigRock = Texture.new( "Graphics/InGame/obstacle_rock2.png" ),
		tileHawk = Texture.new( "Graphics/InGame/obstacle_hawk.png" ),
		tileShark = Texture.new( "Graphics/InGame/obstacle_shark.png" ),
		tileBomb = Texture.new( "Graphics/InGame/obstacle_bomb.png" ),
		tileEgret = Texture.new( "Graphics/InGame/obstacle_egret.png" ),
		tileHawk = Texture.new( "Graphics/InGame/obstacle_hawk.png" ),
		tileHippo = Texture.new( "Graphics/InGame/obstacle_hippo.png" ),
		tileOctopus = Texture.new( "Graphics/InGame/obstacle_octopus.png" ),
		tileOwl = Texture.new( "Graphics/InGame/obstacle_owl.png" ),
		tileSnake = Texture.new( "Graphics/InGame/obstacle_snake.png" ),
	}
	
	self.background = Bitmap.new( self.textures.background )
	
	self.images = {}
	self.labels = {
		header = TextField.new( fontManager.header, languageManager:GetString( "ChooseATile" ) ),
	}
	self.labels.header:setTextColor( 0xffffff )
	
	self.buttons = {
		back = Bitmap.new( Texture.new( "Graphics/UserInterface/editor_tileselect_back.png" ) ),
	}
	self.buttons.back:setPosition( 0, 0 )
	
	TileSelectState.tiles = {
		empty = { 
			bitmap = Bitmap.new( self.textures.tileEmpty ),
			name = "empty",
			texture = nil
		},
		ending  = {
			bitmap = Bitmap.new( self.textures.tileEnd ),
			name = "ending",
			texture = self.textures.tileEnd,
			movement = "static",
		},		
		heart  = {
			bitmap = Bitmap.new( self.textures.tileHeart ),
			name = "heart",
			texture = self.textures.tileHeart,
			movement = "slowfloatup",
		},
		
		-- Good
		-- Beach
		bell  = {
			bitmap = Bitmap.new( self.textures.tileBell ),
			name = "bell",
			texture = self.textures.tileBell,
			movement = "static",
		},
		fish  = {
			bitmap = Bitmap.new( self.textures.tileFish ),
			name = "fish",
			texture = self.textures.tileFish,
			movement = "hop",
		},
		balloon  = {
			bitmap = Bitmap.new( self.textures.tileBalloon ),
			name = "balloon",
			texture = self.textures.tileBalloon,
			movement = "floatup",
		},
		butterfly  = {
			bitmap = Bitmap.new( self.textures.tileButterfly ),
			name = "butterfly",
			texture = self.textures.tileButterfly,
			movement = "fasthorizontal",
		},
			
		
		-- Bad
		-- Beach
		smallrock  = {
			bitmap = Bitmap.new( self.textures.tileSmallRock ),
			name = "smallrock",
			texture = self.textures.tileSmallRock,
			movement = "static",
		},
		hawk  = {
			bitmap = Bitmap.new( self.textures.tileHawk ),
			name = "hawk",
			texture = self.textures.tileHawk,
			movement = "sinewave",
		},
		shark  = {
			bitmap = Bitmap.new( self.textures.tileShark ),
			name = "shark",
			texture = self.textures.tileShark,
			movement = "fasthorizontal",
		},
		
	}
	
	self.row = 1
	self.column = 1
	self:PlaceBrush( TileSelectState.tiles.empty.bitmap )
	self:PlaceBrush( TileSelectState.tiles.heart.bitmap )
	self:PlaceBrush( TileSelectState.tiles.ending.bitmap )
	
	self.column = 1
	self.row = 2
	-- BEACH - good
	self:PlaceBrush( TileSelectState.tiles.bell.bitmap )
	self:PlaceBrush( TileSelectState.tiles.fish.bitmap )
	self:PlaceBrush( TileSelectState.tiles.balloon.bitmap )
	self:PlaceBrush( TileSelectState.tiles.butterfly.bitmap )
	
	-- BEACH - bad
	self:PlaceBrush( TileSelectState.tiles.smallrock.bitmap )
	self:PlaceBrush( TileSelectState.tiles.hawk.bitmap )
	self:PlaceBrush( TileSelectState.tiles.shark.bitmap )
	
	
	
	
	self.currentBrush = self.textures.tileBell.bitmap
	self.currentBrushName = self.textures.tileBell.name
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function TileSelectState:PlaceBrush( brush )
	local increment = 60
	brush:setPosition( increment * self.column - 20, increment * self.row + 10 ) 
	
	self.column = self.column + 1	
	if self.column == 8 then
		self.column = 1
		self.row = self.row + 1
	end
end

function TileSelectState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function TileSelectState:Event_MouseClick( event )
	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "editor", false )	
	
	elseif TileSelectState.tiles.empty.bitmap:hitTestPoint( event.x, event.y ) then
		audioManager.gettea:play()
		TileSelectState.currentBrush = nil
		StateManager:StateSetup( "editor", false )	
	end
		
	for key, tile in pairs( TileSelectState.tiles ) do		
		if tile.bitmap:hitTestPoint( event.x, event.y ) and tile.name ~= "empty" then
			audioManager.gettea:play()
			TileSelectState.currentBrush = tile.texture
			TileSelectState.currentBrushName = tile.name
			TileSelectState.currentBrushMovement = tile.movement
			StateManager:StateSetup( "editor", false )	
		end
	end
end

function TileSelectState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "editor", false )
	end
end

function TileSelectState:UpdateLabels()
	for i = 1, 8 do
		local index = i + 1
		local scrollLabel = i + self.scrollAmount
		self.labels[ i ]:setText( scrollLabel )
		self.labels[ i ]:setPosition( index * 40, 15 )
		self.labels[ i ]:setTextColor( 0x5ba0ff )
	end
end

function TileSelectState:Update()
end

function TileSelectState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( TileSelectState.tiles ) do
		stage:addChild( value.bitmap )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function TileSelectState:Clear()
	stage:removeChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( TileSelectState.tiles ) do
		stage:removeChild( value.bitmap )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function TileSelectState:ReloadLanguage()
	local pos = SCREEN_WIDTH / 2 - string.len( languageManager:GetString( "ChooseATile" ) ) * fontManager.headerWidth / 2
	self.labels.header:setText( languageManager:GetString( "ChooseATile" ) )
	self.labels.header:setPosition( pos, 30 )
end
	