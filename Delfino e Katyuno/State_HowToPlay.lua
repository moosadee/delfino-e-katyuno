HowToPlayState = Core.class()

function HowToPlayState:init( options )
	self.name = options.name
end

function HowToPlayState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_title.png" ),
		subbackground = Texture.new( "Graphics/UserInterface/shade_frame.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" ),
		buttonforward = Texture.new( "Graphics/UserInterface/button_forward.png" ),
		help1 = Texture.new( "Graphics/UserInterface/help_01.png" ),
		help2 = Texture.new( "Graphics/UserInterface/help_02.png" ),
		help3 = Texture.new( "Graphics/UserInterface/help_03.png" ),
		help4 = Texture.new( "Graphics/UserInterface/help_04.png" ),
	}
	self.background = Bitmap.new( self.textures.background )
	self.subbackground = Bitmap.new( self.textures.subbackground )
	self.subbackground:setPosition( 10, 10 )
	
	self.images = {
		help1 = Bitmap.new( self.textures.help1 ),
		help2 = Bitmap.new( self.textures.help2 ),
	}
	self.images.help1:setPosition( 25, 10 )
	self.images.help2:setPosition( 25, 130 )
	--self.images.help3:setPosition( 30, 230 )
	--self.images.help4:setPosition( 30, 330 )
	
	self.buttons = {
		back = Bitmap.new( self.textures.buttonback ),
		forward = Bitmap.new( self.textures.buttonforward ),
	}
	self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	self.buttons.forward:setPosition( SCREEN_WIDTH - self.textures.buttonforward:getWidth(), SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	
	self.scroll = 0
	
	self.labels = {
		help1 = TextField.new( fontManager.small, languageManager:GetString( "Help1" ) ),
		help2 = TextField.new( fontManager.small, languageManager:GetString( "Help2" ) ),
		help3 = TextField.new( fontManager.small, languageManager:GetString( "Help3" ) ),
		help4 = TextField.new( fontManager.small, languageManager:GetString( "Help4" ) ),
		help5 = TextField.new( fontManager.small, languageManager:GetString( "Help5" ) ),		
		help6 = TextField.new( fontManager.small, languageManager:GetString( "Help6" ) ),		
		help7 = TextField.new( fontManager.small, languageManager:GetString( "Help7" ) ),		
	}
	
	local x, y, inc = 110, 30, 25
	for key, value in pairs( self.labels ) do 
		value:setTextColor( 0xffffff ) 
	end

	self.labels.help1:setPosition( x, y ); y = y + inc
	self.labels.help2:setPosition( x, y ); y = y + inc
	self.labels.help3:setPosition( x, y ); y = y + inc
	self.labels.help4:setPosition( x, y ); y = y + inc
	
	y = 150
	self.labels.help5:setPosition( x, y ); y = y + inc
	self.labels.help6:setPosition( x, y ); y = y + inc
	self.labels.help7:setPosition( x, y ); y = y + inc
	
	-- credits

	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function HowToPlayState:TurnPage( direction )
	self.scroll = self.scroll + direction
	if self.scroll < 0 then
		StateManager:StateSetup( "help", false )
		
	else
		if self.scroll == 0 then
			self.images.help1:setTexture( self.textures.help1 )
			self.images.help2:setTexture( self.textures.help2 )	
			
			self.labels.help1:setText( languageManager:GetString( "Help1" ) )
			self.labels.help2:setText( languageManager:GetString( "Help2" ) )
			self.labels.help3:setText( languageManager:GetString( "Help3" ) )
			self.labels.help4:setText( languageManager:GetString( "Help4" ) )
			self.labels.help5:setText( languageManager:GetString( "Help5" ) )		
			self.labels.help6:setText( languageManager:GetString( "Help6" ) )		
			self.labels.help7:setText( languageManager:GetString( "Help7" ) )
			
		elseif self.scroll == 1 then		
			self.images.help1:setTexture( self.textures.help3 )
			self.images.help2:setTexture( self.textures.help4 )
			
			self.labels.help1:setText( languageManager:GetString( "Help8" ) )
			self.labels.help2:setText( languageManager:GetString( "Help9" ) )
			self.labels.help3:setText( "" )
			self.labels.help4:setText( "" )
			self.labels.help5:setText( languageManager:GetString( "Help10" ) )
			self.labels.help6:setText( languageManager:GetString( "Help11" ) )
			self.labels.help7:setText( "" )
			
		elseif self.scroll == 2 then
			StateManager:StateSetup( "howtoplay2", false )	
			
		end
	
	end
end

function HowToPlayState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:removeEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function HowToPlayState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "title" )
	end
end

function HowToPlayState:Update()
end

function HowToPlayState:Draw()
	stage:addChild( self.background )
	stage:addChild( self.subbackground )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function HowToPlayState:Clear()
	stage:removeChild( self.background )
	stage:removeChild( self.subbackground )
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function HowToPlayState:ReloadLanguage()	
	--self.labels.play:setText( languageManager:GetString( "Play" ) )
end

function HowToPlayState:Event_MouseClick( event )
	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		self:TurnPage( -1 )
		
	elseif self.buttons.forward:hitTestPoint( event.x, event.y ) then
		self:TurnPage( 1 )
		
	end
end