PlayMenuState = Core.class()

function PlayMenuState:init( options )
	self.name = options.name	
end

function PlayMenuState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_title.png" ),
		buttonbg = Texture.new( "Graphics/UserInterface/button_bg.png" ),
		buttonbgdisabled = Texture.new( "Graphics/UserInterface/button_bg_disabled.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" )
	}
	
	self.background = Bitmap.new( self.textures.background )
	
	self.buttons = {
		story = Bitmap.new( self.textures.buttonbg ),
		challenge = Bitmap.new( self.textures.buttonbgdisabled ),
		editor = Bitmap.new( self.textures.buttonbg ),
		battle = Bitmap.new( self.textures.buttonbgdisabled ),
		custom = Bitmap.new( self.textures.buttonbg ),
		--back = Bitmap.new( self.textures.buttonback ),
	}
	
	self.buttonbgX = SCREEN_WIDTH/2 - self.textures.buttonbg:getWidth()/2
	local gridVert = SCREEN_HEIGHT / 11
	
	local pos = 1
	self.buttons.story:setPosition( self.buttonbgX, gridVert * pos ); pos = pos + 2
	self.buttons.custom:setPosition( self.buttonbgX, gridVert * pos ); pos = pos + 2
	self.buttons.editor:setPosition( self.buttonbgX, gridVert * pos ); pos = pos + 2
	self.buttons.challenge:setPosition( self.buttonbgX, gridVert * pos ); pos = pos + 2
	self.buttons.battle:setPosition( self.buttonbgX, gridVert * pos ); pos = pos + 2
	--self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	
	self.labels = {
		story = TextField.new( fontManager.button, languageManager:GetString( "Story" ) ),
		challenge = TextField.new( fontManager.button, languageManager:GetString( "Challenge" ) ),
		custom = TextField.new( fontManager.button, languageManager:GetString( "CustomLevels" ) ),
		editor = TextField.new( fontManager.button, languageManager:GetString( "Editor" ) ),
		battle = TextField.new( fontManager.button, languageManager:GetString( "Battle" ) )
	}
	self.labels.story:setTextColor( 0xffffff )
	self.labels.challenge:setTextColor( 0xffffff )
	self.labels.editor:setTextColor( 0xffffff )
	self.labels.battle:setTextColor( 0xffffff )
	self.labels.custom:setTextColor( 0xffffff )
	
	self.labelPositions = {
		fontSize = fontManager.buttonWidth
	}
	pos = 1
	self.labelPositions.story = 	{ x = 10, y = gridVert * pos + 20 }; pos = pos + 2
	self.labelPositions.custom = 	{ x = 10, y = gridVert * pos + 20 }; pos = pos + 2
	self.labelPositions.editor = 	{ x = 10, y = gridVert * pos + 20 }; pos = pos + 2
	self.labelPositions.challenge = { x = 10, y = gridVert * pos + 20 }; pos = pos + 2
	self.labelPositions.battle = 	{ x = 10, y = gridVert * pos + 20 }; pos = pos + 2
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function PlayMenuState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:removeEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function PlayMenuState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "title" )
	end
end

function PlayMenuState:ReloadLanguage()
	self.labelPositions.story.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Story" ) ) / 2
	self.labelPositions.challenge.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Challenge" ) ) / 2
	self.labelPositions.editor.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Editor" ) ) / 2
	self.labelPositions.battle.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Battle" ) ) / 2
	self.labelPositions.custom.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "CustomLevels" ) ) / 2

	self.labels.story:setText( languageManager:GetString( "Story" ) )
	self.labels.challenge:setText( languageManager:GetString( "Challenge" ) )
	self.labels.editor:setText( languageManager:GetString( "Editor" ) )
	self.labels.battle:setText( languageManager:GetString( "Battle" ) )
	self.labels.custom:setText( languageManager:GetString( "CustomLevels" ) )
	
	self.labels.story:setPosition( self.labelPositions.story.x, self.labelPositions.story.y )
	self.labels.challenge:setPosition( self.labelPositions.challenge.x, self.labelPositions.challenge.y )
	self.labels.editor:setPosition( self.labelPositions.editor.x, self.labelPositions.editor.y )
	self.labels.battle:setPosition( self.labelPositions.battle.x, self.labelPositions.battle.y )
	self.labels.custom:setPosition( self.labelPositions.custom.x, self.labelPositions.custom.y )
end

function PlayMenuState:Update()
end

function PlayMenuState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function PlayMenuState:Clear()
	stage:removeChild( self.background )
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function PlayMenuState:Event_MouseClick( event )	
	if self.buttons.story:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "levelselect" )
	
	elseif self.buttons.challenge:hitTestPoint( event.x, event.y ) then
	
	elseif self.buttons.editor:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "editor" )
	
	elseif self.buttons.custom:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "customloader" )
	
	elseif self.buttons.battle:hitTestPoint( event.x, event.y ) then
	
	--elseif self.buttons.back:hitTestPoint( event.x, event.y ) then
	--	StateManager:StateSetup( "title" )
	
	end
end
