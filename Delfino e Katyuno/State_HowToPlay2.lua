HowToPlay2State = Core.class()

function HowToPlay2State:init( options )
	self.name = options.name
end

function HowToPlay2State:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_title.png" ),
		subbackground = Texture.new( "Graphics/UserInterface/shade_frame.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" ),
		buttonforward = Texture.new( "Graphics/UserInterface/button_forward.png" ),
	}
	self.background = Bitmap.new( self.textures.background )
	self.subbackground = Bitmap.new( self.textures.subbackground )
	self.subbackground:setPosition( 10, 10 )
	
	self.images = {
		collect = Bitmap.new( Texture.new( "Graphics/UserInterface/collectables.png" ) ),
		avoid = Bitmap.new( Texture.new( "Graphics/UserInterface/avoidables.png" ) ),
	}
	self.images.collect:setPosition( 50, 50 )
	self.images.avoid:setPosition( 50, 180 )

	
	self.buttons = {
		back = Bitmap.new( self.textures.buttonback ),
		forward = Bitmap.new( self.textures.buttonforward ),
	}
	self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	self.buttons.forward:setPosition( SCREEN_WIDTH - self.textures.buttonforward:getWidth(), SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	
	self.scroll = 0
	
	self.labels = {
		help1 = TextField.new( fontManager.main, languageManager:GetString( "CollectThese" ) ),
		help2 = TextField.new( fontManager.main, languageManager:GetString( "AvoidThese" ) ),	
	}
	
	for key, value in pairs( self.labels ) do 
		value:setTextColor( 0xffffff ) 
	end

	self.labels.help1:setPosition( 15, 30 )
	self.labels.help2:setPosition( 15, 150 )
	
	-- credits

	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function HowToPlay2State:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:removeEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function HowToPlay2State:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "title" )
	end
end

function HowToPlay2State:Update()
end

function HowToPlay2State:Draw()
	stage:addChild( self.background )
	stage:addChild( self.subbackground )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function HowToPlay2State:Clear()
	stage:removeChild( self.background )
	stage:removeChild( self.subbackground )
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function HowToPlay2State:ReloadLanguage()	
	--self.labels.play:setText( languageManager:GetString( "Play" ) )
end

function HowToPlay2State:Event_MouseClick( event )
	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "howtoplay", false )
		
	end
end