AudioManager = Core.class()

function AudioManager:init()
	self.waves = Sound.new( "Audio/wave.wav" )
	self.getbell = Sound.new( "Audio/getbell.wav" )
	self.gettea = Sound.new( "Audio/gettea.wav" )
	self.cancel = Sound.new( "Audio/cancel.wav" )
	self.catjump = Sound.new( "Audio/catjump.wav" )
	self.dolphinjump = Sound.new( "Audio/dolphinjump.wav" )
	
	self.musicTitle = Sound.new( "Audio/TunaFish.mp3" )
	self.musicLevelA = Sound.new( "Audio/Spaced.mp3" )
	self.musicLevelB = Sound.new( "Audio/TippinOfCows.mp3" )
	self.musicLevelC = Sound.new( "Audio/SuburbanDinosaur.mp3" )
	
	self.currentSong = ""
	self.musicChannel = nil
end

