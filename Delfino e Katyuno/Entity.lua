Entity = Core.class( Sprite )

function Entity:init()
end

function Entity:Setup( options )
	self.bitmap = Bitmap.new( options.texture )
	self.position = { x = options.x, y = options.y }
	self.originalPosition = { x = options.x, y = options.y }
	self.dimensions = { width = options.width, height = options.height }
	
	self.active = true
	self.type = options.type
	self.affect = {}
	
	self.movement = options.movement
	self:SetupType()
	
	if options.speed ~= nil then
		self.speed = options.speed
	end
	
	self.gravity = 0.25
	self.falling = false
	self.velocityY = 0
	self.maxY = SCREEN_HEIGHT - 90
	self.movementCounter = 0
	self.movementCounterMax = 20
	self.verticalMovement = 0
	
	self.bitmap:setPosition( self.position.x, self.position.y )
	--self.bitmap:addEventListener( Event.ENTER_FRAME, self.Update, self )	
end

function Entity:SetupType()
	if self.type == "bell" then 
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "fish" then
		self.affect.score = 5
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "balloon" then 
		self.affect.score = 20
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "butterfly" then
		self.affect.score = 15
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "tea" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "sun" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "candy" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "penguin" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "lotus" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "acorn" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "feather" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	elseif self.type == "frog" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.trinket = true
	
	
	
	
	elseif self.type == "smallrock" then
		self.affect.score = -10
		self.affect.damage = true
	
	elseif self.type == "hawk" then
		self.affect.score = -5
		self.affect.damage = true
	
	elseif self.type == "shark" then
		self.affect.score = -5
		self.affect.damage = true
	
	elseif self.type == "bomb" then
		self.affect.score = -10
		self.affect.damage = true
	
	elseif self.type == "octopus" then
		self.affect.score = -10
		self.affect.damage = true
	
	elseif self.type == "owl" then
		self.affect.score = -10
		self.affect.damage = true
	
	elseif self.type == "egret" then
		self.affect.score = -10
		self.affect.damage = true
	
	elseif self.type == "hippo" then
		self.affect.score = -10
		self.affect.damage = true
	
	elseif self.type == "snake" then
		self.affect.score = -10
		self.affect.damage = true
		
	
	elseif self.type == "heart" then
		self.affect.score = 10
		self.affect.damage = false
		self.affect.life = true
	
	end


	self.speed = -3 -- default
		
	if self.movement == "static" then
		self.speed = -3
	
	elseif self.movement == "circle" then
	
	elseif self.movement == "sinewave" then
		self.verticalMovement = 4
		self.speed = -4
	
	elseif self.movement == "fasthorizontal" then
		self.speed = -5
	
	elseif self.movement == "floatup" then
		self.speed = -3
	
	elseif self.movement == "hop" then
	
	elseif self.movement == "netosw" then -- NE to SW
	
	end
end

function Entity:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.bitmap:removeEventListener( Event.ENTER_FRAME, self.Update, self )	
end

function Entity:Deactivate()
	self.active = false
	self:Clear()
end

function Entity:Update( scroll )
	-- Begin moving
	self.bitmap:setPosition( self.position.x, self.position.y )
	
	
	self.movementCounter = self.movementCounter + 1
	
	if self.movementCounter > self.movementCounterMax then
		self.movementCounter = 0 
	end
	
	
	if self.movement == "static" then
		--self.position.x = self.position.x + self.speed
	
	elseif self.movement == "circle" then
	
	
	elseif self.movement == "sinewave" then
		--self.originalPosition
		self.position.y = self.originalPosition.y + 20 * math.sin( ( scroll * 0.10 ) % 6.28 )
	
	
	elseif self.movement == "fasthorizontal" then
		--self.position.x = self.position.x + self.speed
	
	elseif self.movement == "floatup" then
		if scroll / 30 >= self.position.x - SCREEN_WIDTH then
			self.position.y = self.position.y - 1
		end
	
	
	elseif self.movement == "hop" then


	elseif self.movement == "netosw" then -- NE to SW
	
	
	end
	
	if scroll / 30 >= self.position.x - SCREEN_WIDTH then
		self.position.x = self.position.x + self.speed
	else
		self.position.x = self.position.x + -3
	end
	
	if self.position.x < -80 then
		self:Deactivate()
	end
end

function Entity:EndLevelHit( playerX )
	return self.type == "ending" and playerX >= self.position.x 
end

function Entity:Draw()
	stage:addChild( self.bitmap )
end

function Entity:Clear()
	if stage:contains( self.bitmap ) then 
		if stage:contains( self.bitmap ) then stage:removeChild( self.bitmap ) end
	end
end

function Entity:Jump()
	if self.jumpCounter <= 0 then
		-- begin jumpCounter
		self.jumpCounter = self.jumpCounterMax
	end
end