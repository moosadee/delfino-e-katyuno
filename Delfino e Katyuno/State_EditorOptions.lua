EditorOptionsState = Core.class()

function EditorOptionsState:init( options )
	self.name = options.name
	EditorOptionsState.currentBrush = nil
	EditorOptionsState.filename = "mea"
	EditorOptionsState.runTest = false
end

function EditorOptionsState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_checker.png" ),
	}
	
	self.background = Bitmap.new( self.textures.background )
	
	local tileCount = 0
	for key, value in pairs( EditorState.tiles ) do
		tileCount = tileCount + 1
	end
	
	self.images = {}
	self.labels = {
		filename = TextField.new( fontManager.button, languageManager:GetString( "Filename" ) .. ": " .. EditorOptionsState.filename .. ".maro" ),
		save = TextField.new( fontManager.button, languageManager:GetString( "Save" ) ),
		load = TextField.new( fontManager.button, languageManager:GetString( "Load" ) ),
		--gametype = TextField.new( fontManager.button, languageManager:GetString( "GameType" ) .. ": " .. languageManager:GetString( "GameTypeHop" ) ),
		tilecount = TextField.new( fontManager.button, languageManager:GetString( "TileCount" ) .. ": " .. tileCount ),
		back = TextField.new( fontManager.button, languageManager:GetString( "BackToMenu" ) ),
		clear = TextField.new( fontManager.button, languageManager:GetString( "ClearLevel" ) ),
		test = TextField.new( fontManager.button, languageManager:GetString( "TestLevel" ) ),
		status = TextField.new( fontManager.small, "..." ),
	}
	
	local index = 1
	local spacing = 40
	
	--self.labels.gametype:setPosition( 100, index * spacing ); index = index + 1
	--self.labels.gametype:setTextColor( 0xffffff )
	
	self.labels.filename:setPosition( 100, index * spacing ); index = index + 1
	self.labels.filename:setTextColor( 0xffffff )
	
	self.labels.save:setPosition( 100, index * spacing )
	self.labels.save:setTextColor( 0xffffff )
	
	self.labels.load:setPosition( 300, index * spacing ); index = index + 1
	self.labels.load:setTextColor( 0xffffff )
	
	self.labels.test:setPosition( 100, index * spacing ); index = index + 2
	self.labels.test:setTextColor( 0xffffff )
	
	self.labels.clear:setPosition( 100, index * spacing ); index = index + 1
	self.labels.clear:setTextColor( 0xffffff )
	
	self.labels.back:setPosition( 100, index * spacing ); index = index + 1
	self.labels.back:setTextColor( 0xffffff )
	
	self.labels.tilecount:setPosition( 10, 290 )
	self.labels.tilecount:setTextColor( 0xffffff )
	
	self.labels.status:setPosition( 10, 310 )
	self.labels.status:setTextColor( 0xffffff )
	
	self.buttons = {
		back = Bitmap.new( Texture.new( "Graphics/UserInterface/editor_tileselect_back.png" ) ),
	}
	self.buttons.back:setPosition( 0, 0 )
	
	if EditorOptionsState.runTest == true then
		self:LoadLevel( true )
		EditorOptionsState.runTest = false
	end
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function EditorOptionsState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function EditorOptionsState:GetDialogReturn( event )
	if event.buttonIndex == 1 then
		EditorOptionsState.filename = event.text
	end

	self.labels.filename:setText( languageManager:GetString( "Filename" ) .. ": " .. EditorOptionsState.filename .. ".maro" )
end

function EditorOptionsState:Event_MouseClick( event )
	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		audioManager.gettea:play()
		StateManager:StateSetup( "editor", false )	
	
	elseif self.labels.back:hitTestPoint( event.x, event.y ) then
		audioManager.gettea:play()
		StateManager:StateSetup( "playmenu", true )		
	
	elseif self.labels.filename:hitTestPoint( event.x, event.y ) then
		audioManager.gettea:play()
		self:Highlight( self.labels.filename )
		-- Ask user to input map name
		local textInputDialog = TextInputDialog.new( languageManager:GetString( "EnterLevelFilename" ), languageManager:GetString( "EnterLevelFilename" ), languageManager:GetString( "DefaultMapName" ), languageManager:GetString( "Cancel" ), languageManager:GetString( "OK" ) )

		textInputDialog:addEventListener( Event.COMPLETE, EditorOptionsState.GetDialogReturn, self )
		textInputDialog:show()
		
		
	elseif self.labels.save:hitTestPoint( event.x, event.y ) then
		audioManager.getbell:play()
		self:Highlight( self.labels.save )
		self:SaveLevel()
		
		
	elseif self.labels.load:hitTestPoint( event.x, event.y ) then
		audioManager.getbell:play()
		self:Highlight( self.labels.load )
		self:LoadLevel()
		
	
	elseif self.labels.clear:hitTestPoint( event.x, event.y ) then		
		audioManager.cancel:play()
		self:Highlight( self.labels.clear )
		self:ClearLevel()		
		
		self.labels.status:setText( languageManager:GetString( "ClearedLevel" ) )
		self.labels.status:setTextColor( 0x00ff00 )
	
	elseif self.labels.test:hitTestPoint( event.x, event.y ) then
		self:SaveLevel( true )
		-- Transition to game state, have it load the temp file,
		-- then return to editor when done.
		EditorOptionsState.runTest = true
		StateManager:StateSetup( "storygame", false )
	
	end
end

function EditorOptionsState:ClearLevel()
		for key, tile in pairs( EditorState.tiles ) do
			-- Remove tile from table, remove from stage
			EditorState.tiles[ key ] = nil
		end
		
		local tileCount = 0
		for key, value in pairs( EditorState.tiles ) do
			tileCount = tileCount + 1
		end
		
		self.labels.tilecount:setText( languageManager:GetString( "TileCount" ) .. ": " .. tileCount )		
end

function EditorOptionsState:Highlight( label )
	for key, value in pairs( self.labels ) do
		value:setTextColor( 0xffffff )
	end
	
	label:setTextColor( 0xff0000 )
end

function EditorOptionsState:SaveLevel( tempMap )
	require "lfs"
	lfs.mkdir("/sdcard/Delfino/")
	local prefix = "/sdcard/Delfino/"
	local filename = ""
	local levelname = EditorOptionsState.filename
	
	if tempMap == true then
		levelname = "test"
	end
	
	filename = prefix .. levelname .. ".maro"

    local destFile = io.open( filename, "wb" )
	
	if destFile == nil then
		prefix = "|D|"
		filename = prefix .. levelname .. ".maro"
		destFile = io.open( filename, "wb" )
	end
	
	
	if destFile == nil then
		self.labels.status:setText( languageManager:GetString( "ErrorOpeningFile" ) .. " '" .. filename .. "'!" )
		self.labels.status:setTextColor( 0xff0000 )
	else
	
		destFile:write( "gametype hop" )
		destFile:write( "\n" )
		
		local count = 1
		for key, tile in pairs( EditorState.tiles ) do
			local line = "index " .. count .. " x " .. tile.position.x .. " y " .. tile.position.y .. " type " .. tile.tiletype .. " movement " .. tile.movement .. " end\n"
			destFile:write( line )
			count = count + 1
		end
		
		self.labels.status:setText( languageManager:GetString( "SavedLevel" ) .. " '" .. filename .. "'!" )
		self.labels.status:setTextColor( 0x00ff00 )
	
    destFile:close()
	end
end

function EditorOptionsState:LoadLevel( tempMap )
	require "lfs"
	lfs.mkdir("/sdcard/Delfino/")
	local prefix = "/sdcard/Delfino/"
	local filename = ""
	filename = prefix .. EditorOptionsState.filename .. ".maro"
		
	local sourceFile = io.open( filename, "rb" )
	
	if sourceFile == nil then
		prefix = "|D|"
		filename = prefix .. EditorOptionsState.filename .. ".maro"
		sourceFile = io.open( filename, "rb" )
	end
	
	if sourceFile == nil then
		self.labels.status:setText( languageManager:GetString( "ErrorOpeningFile" ) .. " '" .. filename .. "'!" )
		self.labels.status:setTextColor( 0xff0000 )
	else			
		self:ClearLevel()
		
		for line in sourceFile:lines() do	
			-- Find whitespace
			local whitespaceIndices = {}
			local count = 0
			for i = 1, string.len( line ) do 				
				local substr = string.sub( line, i, i )				
				if substr == " " then
					table.insert( whitespaceIndices, i )
					count = count + 1
				end	
			end		
			count = count + 1
			whitespaceIndices[ count ] = string.len( line ) + 1
			
			-- Read each command
			local localx, localy, type, localmov = nil, nil, nil, nil
			local position = 1
			local lastCommand = nil
			for i = 1, count do
				local substr = string.sub( line, position, whitespaceIndices[i]-1 )
				position = whitespaceIndices[i] + 1
				
				if lastCommand == "x" then
					localx = substr
				
				elseif lastCommand == "y" then
					localy = substr
				
				elseif lastCommand == "type" then
					type = substr
					
				elseif lastCommand == "movement" then
					localmov = substr
					
				elseif substr == "end" then
					-- Save					
					local tileInfo = { 
						tiletype = type,
						position = { x = localx, y = localy },
						movement = localmov
					}
					
					-- TODO: Trying to load when there are no files causes this to be nil
					if TileSelectState.tiles == nil then
						self.labels.status:setText( languageManager:GetString( "ErrorOpeningFile" ) .. " '" .. filename .. "'!" )
						self.labels.status:setTextColor( 0xff0000 )
						return
					end
					
					for key, brush in pairs( TileSelectState.tiles ) do
						if brush.name == tileInfo.tiletype then
							tileInfo.bitmap = Bitmap.new( brush.texture )
						end
					end
					
					local locx = (tileInfo.position.x + 1) * 40
					local locy = (tileInfo.position.y - 1) * 40
					tileInfo.bitmap:setPosition( locx, locy )
					
					table.insert( EditorState.tiles, tileInfo )
				
				end
				
				lastCommand = substr
			end
			
		end
		
		self.labels.status:setText( languageManager:GetString( "LoadedLevel" ) .. " '" .. filename .. "'!" )
			self.labels.status:setTextColor( 0x00ff00 )
		
		sourceFile:close()
	
	end
	
	self:ReloadLanguage()
end

function EditorOptionsState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "editor", false )
	end
end

function EditorOptionsState:Update()
end

function EditorOptionsState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function EditorOptionsState:Clear()
	stage:removeChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function EditorOptionsState:ReloadLanguage()
	self.labels.filename:setText( languageManager:GetString( "Filename" ) .. ": " .. EditorOptionsState.filename .. ".maro" )
	self.labels.save:setText( languageManager:GetString( "Save" ) )
	self.labels.load:setText( languageManager:GetString( "Load" ) )
	--self.labels.gametype:setText( languageManager:GetString( "GameType" ) .. ": " .. languageManager:GetString( "GameTypeHop" ) )
	self.labels.clear:setText( languageManager:GetString( "ClearLevel" ) )
	self.labels.back:setText( languageManager:GetString( "BackToMenu" ) )
		
	local tileCount = 0
	for key, value in pairs( EditorState.tiles ) do
		tileCount = tileCount + 1
	end
	self.labels.tilecount:setText( languageManager:GetString( "TileCount" ) .. ": " .. tileCount )
end
	