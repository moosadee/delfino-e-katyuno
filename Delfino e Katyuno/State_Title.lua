TitleState = Core.class()

function TitleState:init( options )
	self.name = options.name
end

function TitleState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_title.png" ),
		text = Texture.new( "Graphics/UserInterface/title_text.png" ),
		buttonbg = Texture.new( "Graphics/UserInterface/button_bg.png" ),
		usflag = Texture.new( "Graphics/UserInterface/usflag.png" ),
		idostar = Texture.new( "Graphics/UserInterface/idostar.png" ), 
		eostar = Texture.new( "Graphics/UserInterface/eostar.png" ), 
		yesmusic = Texture.new( "Graphics/UserInterface/music-icon.png" ), 
		nomusic = Texture.new( "Graphics/UserInterface/no-music-icon.png" ), 
	}
	
	self.background = Bitmap.new( self.textures.background )
	
	self.images = {
		title = Bitmap.new( self.textures.text )
	}
	
	self.floatY = 0
	self.floatDirection = 1
	
	self.images.title:setPosition( SCREEN_WIDTH/2 - self.textures.text:getWidth()/2, 5 + self.floatY )
	
	
	self.buttons = {
		play = Bitmap.new( self.textures.buttonbg ),
		help = Bitmap.new( self.textures.buttonbg ),
		--options = Bitmap.new( self.textures.buttonbg ),
		ido = Bitmap.new( self.textures.idostar ),
		en = Bitmap.new( self.textures.usflag ),
		eo = Bitmap.new( self.textures.eostar ),
		music = Bitmap.new( self.textures.yesmusic ),
	}
	
	self.buttonbgX = SCREEN_WIDTH/2 - self.textures.buttonbg:getWidth()/2
	local gridVert = SCREEN_HEIGHT / 20
	
	self.buttons.play:setPosition( self.buttonbgX, gridVert * 12 )
	self.buttons.help:setPosition( self.buttonbgX, gridVert * 15 )
	--self.buttons.options:setPosition( self.buttonbgX, gridVert * 16 )
	
	self.buttons.ido:setPosition( self.buttonbgX + self.textures.buttonbg:getWidth() + 25, gridVert * 12 )
	self.buttons.en:setPosition( self.buttonbgX + self.textures.buttonbg:getWidth() + 25, gridVert * 15 )
	self.buttons.eo:setPosition( self.buttonbgX + self.textures.buttonbg:getWidth() + 75, gridVert * 15 )
	
	self.buttons.music:setPosition( self.buttonbgX - 25 - 32, gridVert * 12 )
	
	self.labels = {
		play = TextField.new( fontManager.main, languageManager:GetString( "Play" ) ),
		help = TextField.new( fontManager.main, languageManager:GetString( "Help" ) ),
		--options = TextField.new( fontManager.main, languageManager:GetString( "Options" ) ),
		credit1 = TextField.new( fontManager.small, "RACHEL J. MORRIS - MOOSADER" ),
		version = TextField.new( fontManager.small, languageManager:GetString( "VERSION" ) ),
		demo = TextField.new( fontManager.main, "DEMO" ),
	}
	
	self.labelPositions = {
		fontSize = fontManager.mainWidth,
		play = { x = 10, y = gridVert * 12 + 20 },
		help = { x = 10, y = gridVert * 15 + 20 },
		--options = { x = 10, y = gridVert * 16 + 20 }
	}
	
	self.labels.play:setTextColor( 0xffffff )
	self.labels.help:setTextColor( 0xffffff )
	--self.labels.options:setTextColor( 0xffffff )
	
	self.labels.credit1:setPosition( 2, SCREEN_HEIGHT - 2 )
	self.labels.credit1:setTextColor( 0x000000 )
	self.labels.version:setPosition( 440, SCREEN_HEIGHT - 2 )
	self.labels.version:setTextColor( 0x000000 )
	self.labels.demo:setTextColor( 0x000000 )
	self.labels.demo:setPosition( 350, 150 )
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function TitleState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:removeEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function TitleState:Update()
	self.floatY = self.floatY + self.floatDirection

	if self.floatY > 2 then
		self.floatDirection = -0.1
	elseif self.floatY < -2 then
		self.floatDirection = 0.1
	end
	
	self.images.title:setPosition( SCREEN_WIDTH/2 - self.textures.text:getWidth()/2, 5 + self.floatY )
end

function TitleState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function TitleState:Clear()
	stage:removeChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function TitleState:ReloadLanguage()
	self.labelPositions.play.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Play" ) ) / 2
	self.labelPositions.help.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Help" ) ) / 2
	--self.labelPositions.options.x = SCREEN_WIDTH / 2 - self.labelPositions.fontSize * string.len( languageManager:GetString( "Options" ) ) / 2

	self.labels.play:setPosition( self.labelPositions.play.x, self.labelPositions.play.y )
	self.labels.help:setPosition( self.labelPositions.help.x, self.labelPositions.help.y )
	--self.labels.options:setPosition( self.labelPositions.options.x, self.labelPositions.options.y )
	
	self.labels.play:setText( languageManager:GetString( "Play" ) )
	self.labels.help:setText( languageManager:GetString( "Help" ) )
	--self.labels.options:setText( languageManager:GetString( "Options" ) )
end

function TitleState:Event_MouseClick( event )
	if self.buttons.en:hitTestPoint( event.x, event.y ) then
		languageManager:SetLanguage( "en" )		
		self:ReloadLanguage()
	
	elseif self.buttons.ido:hitTestPoint( event.x, event.y ) then
		languageManager:SetLanguage( "ido" )		
		self:ReloadLanguage()
	
	elseif self.buttons.eo:hitTestPoint( event.x, event.y ) then
		languageManager:SetLanguage( "eo" )		
		self:ReloadLanguage()
	
	elseif self.buttons.play:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "playmenu" )
	
	elseif self.buttons.help:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "help" )
		
	elseif self.buttons.music:hitTestPoint( event.x, event.y ) then
		StateManager:ToggleMusic()
		
		if StateManager.mute == true then
			self.buttons.music:setTexture( self.textures.nomusic )
			
		else
			self.buttons.music:setTexture( self.textures.yesmusic )
			
		end
	
	end
end

