SplashState = Core.class()

function SplashState:init( options )
	self.name = options.name
end

function SplashState:Setup()
	self.textures = {
		splash = Texture.new( "Graphics/UserInterface/background_splash.png" ),
		demo = Texture.new( "Graphics/UserInterface/background_demo.png" ),
		black = Texture.new( "Graphics/UserInterface/background_black.png" ),
	}
	
	self.images = {}
	self.images.splash1 = Bitmap.new( self.textures.splash )
	self.images.splash1:setAlpha( 0 )
	self.images.splash2 = Bitmap.new( self.textures.demo )
	self.images.splash2:setAlpha( 0 )
	self.background = Bitmap.new( self.textures.black )
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	
	self.counterMax = 240
	self.counter = self.counterMax
end

function SplashState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
end

function SplashState:Update()
	if self.counter > 230 then
		local al = self.images.splash1:getAlpha()
		self.images.splash1:setAlpha( al + 0.1 )
		
	elseif self.counter <= 120 and self.counter > 110 then
		local al = self.images.splash1:getAlpha()
		self.images.splash1:setAlpha( al - 0.1 )
	
	elseif self.counter <= 110 and self.counter > 100 then
		local al = self.images.splash2:getAlpha()
		self.images.splash2:setAlpha( al + 0.1 )
	
	elseif self.counter <= 10 and self.counter > 0 then
		local al = self.images.splash2:getAlpha()
		self.images.splash2:setAlpha( al - 0.1 )

	elseif self.counter == 0 then
		StateManager:StateSetup( "title", false )
	end
	
	self.counter = self.counter - 1
end

function SplashState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
end

function SplashState:Clear()
	stage:removeChild( self.background )
	
	for key, value in pairs( self.images ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
end

function SplashState:ReloadLanguage()
end

