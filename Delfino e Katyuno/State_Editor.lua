EditorState = Core.class()

function EditorState:init( options )
	self.name = options.name
	EditorState.tiles = {}
	EditorState.scrollAmount = 0
end

function EditorState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/InGame/background_sky.png" ),
		waves = Texture.new( "Graphics/InGame/tile_wave.png" ),
		waves2 = Texture.new( "Graphics/InGame/tile_wave2.png" ),
		shade = Texture.new( "Graphics/UserInterface/shade.png" ),
	}
	
	self.background = Bitmap.new( self.textures.background )
	self.grid = Bitmap.new( Texture.new( "Graphics/UserInterface/grid.png" ) )
	
	self.images = {
		shadeleft = Bitmap.new( self.textures.shade ),
		shaderight = Bitmap.new( self.textures.shade ),
	}
	
	self.images.shadeleft:setPosition( 0, 0 )
	self.images.shaderight:setPosition( SCREEN_WIDTH - 60, 0 )
	
	self.buttons = {
		forward = Bitmap.new( Texture.new( "Graphics/UserInterface/editor_forward.png" ) ),
		back = Bitmap.new( Texture.new( "Graphics/UserInterface/editor_back.png" ) ),
		options = Bitmap.new( Texture.new( "Graphics/UserInterface/editor_options.png" ) ),
		tileselect = Bitmap.new( Texture.new( "Graphics/UserInterface/editor_tile.png" ) ),
	}

	self.buttons.back:setPosition( 0, SCREEN_HEIGHT - 60 )
	self.buttons.forward:setPosition( SCREEN_WIDTH - 60, SCREEN_HEIGHT - 60 )
	self.buttons.options:setPosition( 0, 0 )
	self.buttons.tileselect:setPosition( SCREEN_WIDTH - 60, 0 )
	
	self.currentBrush = Bitmap.new( Texture.new( "Graphics/UserInterface/tile_empty.png" ) )
	self.currentBrush:setPosition( SCREEN_WIDTH - 40, 0 )
	
	if TileSelectState.currentBrush ~= nil then
		self.currentBrush:setTexture( TileSelectState.currentBrush )
	end
	
	self.labels = {}
	
	for i = 1, 8 do
		local index = i + 1
		self.labels[ i ] = TextField.new( fontManager.small, i )
		self.labels[ i ]:setPosition( index * 40, 15 )
		self.labels[ i ]:setTextColor( 0x5ba0ff )
	end
	
	self.waves1max = 12
	self.waves2max= 23
	
	for i = 1, self.waves1max do
		self.images[i] = Bitmap.new( self.textures.waves )
		local index = i - 1
		local width = 48
		self.images[i]:setPosition( index * width - ( 1 % 80 ), SCREEN_HEIGHT-50 )
	end
	for i = self.waves1max+1, self.waves2max do
		self.images[i] = Bitmap.new( self.textures.waves2 )
		local index = (i - 1) - self.waves1max
		local width = 60
		self.images[i]:setPosition( index * width - ( 1 * 1.25 % 100 ), SCREEN_HEIGHT-15 )
	end
	
	self.scrollSpeed = 1
	self.currentTile = 0
	
	self:Clear()
	
	local count = 0
	for k, v in pairs( EditorState.tiles ) do
		count = count + 1
	end
	
	self:UpdateLabels()
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	self.background:addEventListener( Event.MOUSE_MOVE, self.Event_MouseClick, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseUp, self )	
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function EditorState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	self.background:removeEventListener( Event.MOUSE_MOVE, self.Event_MouseClick, self )	
	self.background:removeEventListener( Event.MOUSE_UP, self.Event_MouseUp, self )	
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function EditorState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "editoroptions", false )
	end
end

function EditorState:Event_MouseUp( event )
	-- Handle the state changing buttons
	if self.buttons.options:hitTestPoint( event.x, event.y ) then		
		StateManager:StateSetup( "editoroptions", false )	
	
	elseif self.buttons.tileselect:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "tileselect", false )	
		
	end
end

function EditorState:Event_MouseClick( event )
	if self.buttons.forward:hitTestPoint( event.x, event.y ) then
		EditorState.scrollAmount = EditorState.scrollAmount + self.scrollSpeed
		self:UpdateLabels()
	
	elseif self.buttons.back:hitTestPoint( event.x, event.y ) then
		EditorState.scrollAmount = EditorState.scrollAmount - self.scrollSpeed
		if EditorState.scrollAmount < 0 then
			EditorState.scrollAmount = 0
		end
		self:UpdateLabels()

	else
		-- take into account x-offset
		
		if TileSelectState.currentBrush == nil then
			-- erase - what tile is already here?
			local localx = ( math.floor( event.x/40 ) + EditorState.scrollAmount - 1 )
			local localy = math.floor( event.y/40 ) + 1
			
			for key, tile in pairs( EditorState.tiles ) do				
				tile.position.x = tonumber( tile.position.x )
				tile.position.y = tonumber( tile.position.y )
			
				if tile.position.x == localx and tile.position.y == localy then
					if stage:contains( tile.bitmap ) then stage:removeChild( tile.bitmap ) end
					EditorState.tiles[ key ] = nil
				end
			end
			
		elseif event.x >= 80 and event.x < SCREEN_WIDTH - 80 and event.y >= 0 and event.y < SCREEN_HEIGHT then
			-- Add new tile or overwrite existing
			local localx = ( math.floor( event.x/40 ) + EditorState.scrollAmount - 1 )
			local localy = math.floor( event.y/40 ) + 1
			
			local newTile = true
			for key, tile in pairs( EditorState.tiles ) do
				if tile.position.x == localx and tile.position.y == localy then
					EditorState.tiles[key].bitmap = Bitmap.new( TileSelectState.currentBrush )
					newTile = false
				end
			end
			
			if newTile == true then
				local tileInfo = { 
					bitmap = Bitmap.new( TileSelectState.currentBrush ),
					tiletype = TileSelectState.currentBrushName,
					movement = TileSelectState.currentBrushMovement,
					position = { x = localx, y = localy }
				}
				tileInfo.bitmap:setPosition( (tileInfo.position.x + 1) * 40, (tileInfo.position.y - 1) * 40 )
				
				table.insert( EditorState.tiles, tileInfo )

				stage:addChild( tileInfo.bitmap )
			end
		end
	end
end

function EditorState:UpdateLabels()
	for i = 1, 8 do
		local index = i + 1
		local scrollLabel = i + EditorState.scrollAmount
		self.labels[ i ]:setText( scrollLabel )
		self.labels[ i ]:setPosition( index * 40, 15 )
		self.labels[ i ]:setTextColor( 0x5ba0ff )
	end
end

function EditorState:Update()
	for key, tile in pairs( EditorState.tiles ) do
		tile.bitmap:setPosition( (tile.position.x + 1 - EditorState.scrollAmount) * 40, (tile.position.y - 1) * 40 )
	end
	self:Draw() -- This prevents the weird "sticky tile" problem.
end

function EditorState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( EditorState.tiles ) do
		stage:addChild( value.bitmap )
	end
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	stage:addChild( self.grid )
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
	
	stage:addChild( self.currentBrush )
end

function EditorState:Clear()
	if stage:contains( self.background ) then stage:removeChild( self.background ) end
	
	for key, value in pairs( self.images ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
	
	for key, value in pairs( self.buttons ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
	
	for key, value in pairs( self.labels ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
	
	for key, value in pairs( EditorState.tiles ) do
		if stage:contains( value.bitmap) then stage:removeChild( value.bitmap ) end
	end
	
	if stage:contains( self.grid ) then stage:removeChild( self.grid ) end
	if stage:contains( self.currentBrush ) then stage:removeChild( self.currentBrush ) end
end

function EditorState:ReloadLanguage()
end
