SCREEN_WIDTH = 480
SCREEN_HEIGHT = 320

languageManager = LanguageManager.new( { language = "en" } )
fontManager = FontManager.new()
audioManager = AudioManager.new()

states = {}
table.insert( states, SplashState.new( { name = "splash" } ) )
table.insert( states, TitleState.new( { name = "title" } ) )
table.insert( states, PlayMenuState.new( { name = "playmenu" } ) )
table.insert( states, LevelSelectState.new( { name = "levelselect" } ) )
table.insert( states, CutsceneState.new( { name = "cutscene" } ) )
table.insert( states, GameStoryState.new( { name = "storygame" } ) )
table.insert( states, EditorState.new( { name = "editor" } ) )
table.insert( states, TileSelectState.new( { name = "tileselect" } ) )
table.insert( states, EditorOptionsState.new( { name = "editoroptions" } ) )
table.insert( states, CustomLoaderState.new( { name = "customloader" } ) )
table.insert( states, HelpState.new( { name = "help" } ) )
table.insert( states, HowToPlayState.new( { name = "howtoplay" } ) )
table.insert( states, HowToPlay2State.new( { name = "howtoplay2" } ) )
table.insert( states, DemoState.new( { name = "demo" } ) )

local transitioner = {
	curtain = Bitmap.new( Texture.new( "Graphics/UserInterface/wavetransition.png" ) ),
	speed = 20,
	state = "inactive",
	counter = SCREEN_HEIGHT,
	nextState = ""
}

local screenFrame = Bitmap.new( Texture.new( "Graphics/UserInterface/screen_frame.png" ) )
screenFrame:setPosition( -68, -40  )

StateManager = Core.class()

function StateManager:init()
	self.currentState = nil
	self.placeholder = Bitmap.new( Texture.new( "Graphics/UserInterface/button_bg.png" ) )
	self.placeholder:setPosition( 1000, 1000 )
	self.placeholder:addEventListener( Event.ENTER_FRAME, self.Update, self )
	
	self.playLevel = 0
	StateManager.gotoLevel = 1
	StateManager.mute = false
end

function StateManager:ToggleMusic()
	
	if StateManager.mute == true then
		StateManager.mute = false
		audioManager.musicChannel = audioManager.musicTitle:play( 0, true )
	else
		StateManager.mute = true
		audioManager.musicChannel:stop()
	end
end

function StateManager:Update( event )
	
	if transitioner.counter >= 0 and transitioner.state == "forward" then
		self:ForwardTransition()
		
	elseif transitioner.counter <= 0 and transitioner.state == "forward" then
		self:NextState( transitioner.nextState )
		transitioner.state = "reverse"
		stage:removeChild( transitioner.curtain, transitioner.state )
		stage:addChild( transitioner.curtain, transitioner.state )
		
	elseif transitioner.counter < SCREEN_HEIGHT and transitioner.state == "reverse" then
		self:ReverseTransition()
		
	elseif transitioner.counter >= SCREEN_HEIGHT and transitioner.state == "reverse" then
		-- End transitioning
		transitioner.state = "inactive"
		stage:removeChild( transitioner.curtain )
	
	end
end

function StateManager:PlayMusic( state )
	if StateManager.mute == true then return end
	if state == "storygame" then
		--if audioManager.currentSong ~= "Spaced" then
			if audioManager.musicChannel ~= nil then audioManager.musicChannel:stop() end
			
			if StateManager.gotoLevel == 1 then
				audioManager.musicChannel = audioManager.musicLevelB:play( 0, true )
			
			elseif StateManager.gotoLevel == 2 or StateManager.gotoLevel == 3 then
				audioManager.musicChannel = audioManager.musicLevelC:play( 0, true )
			
			else
				audioManager.musicChannel = audioManager.musicLevelA:play( 0, true )
			
			end
			
			
			audioManager.currentSong = "Spaced"
		--end
	
	elseif state ~= "splash" then
			
		if audioManager.currentSong ~= "TunaFish" then
			if audioManager.musicChannel ~= nil then audioManager.musicChannel:stop() end
			audioManager.musicChannel = audioManager.musicTitle:play( 0, true )
			
			audioManager.currentSong = "TunaFish"
		end
	
	end
end

function StateManager:StateSetup( state, transition )
	-- Set Music		
	self:PlayMusic( state )
	
	if transition ~= nil and transition == false then		
	
		self:NextState( state )
	else
		-- Begin transition animation
		transitioner.state = "forward"
		transitioner.counter = SCREEN_HEIGHT
		transitioner.nextState = state
		transitioner.curtain:setPosition( 0, SCREEN_HEIGHT )
		stage:addChild( transitioner.curtain )
		audioManager.waves:play()
	end
end

function StateManager:NextState( state )
	if self.currentState ~= nil then
		self.currentState:BreakDown()
	end
	
	self.currentState = nil
	for key, value in pairs( states ) do
		if value.name == state then
			self.currentState = value
		end
	end
	
	if self.currentState == nil then
		print( "ERROR: Could not find state ", state )
	else
		self.currentState:Setup()
		self.currentState:ReloadLanguage()
		self.currentState:Draw()
		--stage:addChild( screenFrame )
	end	
end

function StateManager:ForwardTransition()
	transitioner.counter = transitioner.counter - transitioner.speed
	transitioner.curtain:setPosition( 0, transitioner.counter )
end

function StateManager:ReverseTransition()
	transitioner.counter = transitioner.counter + transitioner.speed
	transitioner.curtain:setPosition( 0, transitioner.counter )
end			
			
StateManager:init()  
--StateManager:StateSetup( "demo", false )
--StateManager:StateSetup( "splash", false )
StateManager:StateSetup( "title", false )
