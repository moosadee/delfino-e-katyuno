# To Do

https://trello.com/b/qidd0Zdc/delfino-katyuno

* Change level select buttons to have thicker outlines, to look better scaled up/down.
* Erase tile isn't working in editor
* End level flag to editor
* Trinket scrolling on non-game states
* Scroller bug after choosing tile
* Launch from map editor to test

End Flag

Countdown Timer

Lose message, Again / Next / Level Select menu

Win message, Again / Next / Level Select menu



## Trinket Types

### Movements...
* Static
* Right->Left, faster movement
* Float Bottom->Top
* Fly Top-Right->Bottom-Left
* Sinewave
* Hop
* Circle
* Towards player

### Beach (Hop)
* Cat bell, +points, static
* Fish, +points, hop (water)
* Balloon, +points, bottom->top
* Butterfly, +points linear-fast (sky)

* Heart, +life, slow float up

* Shark, -life, linear-fast (water)
* Rock, -life, static
* Hawk, -life, shallow sine-wave (sky)


## Antarctic (Hop)
* Tea, +points, static
* Sun, +points, sinewave
* Penguin, +points, hop
* Candy, +points, circle

* Heart, +life, slow float up

* Bomb, -life, Top-Right->Bottom-Left
* Snow Owl, -life, linear-fast
* Octopus, -life, sinewave (water)


## Swamp
* Lotus, +points, static (water)
* Flamingo Feather
* Frog
* Acorn

* Heart, +life, slow float up

* Snake
* Hippo
* Egret


## Volcano
* 
* 
* 
* 
 
* Heart, +life, slow float up

* 
* 
* 

### Sky (SHMUP)


### Underwater (SHMUP)


## Outerspace
